﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryProcessor.DashboardQueryProcessor
{
    public interface IDashboardProcessor
    {
        AttendanceReport GetAllData(string ComCode);
        IEnumerable<OnLeaveEmployee> GetEmployeeOnLeave(string ComCode);
        LeaveReport GetLeaveReport(string ComCode);
        LeaveReport GetLeaveReportWeekly(string ComCode);
        LeaveReport GetLeaveReportMonthly(string ComCode);
    }
}
