﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using Entity;
using Entity.Model;

namespace QueryProcessor
{
    public class AttendanceQueryProcessor : IAttendanceQueryProcessor
    {
        public IEnumerable<AbsentEmployees> GetAbsentEmployess(int days, string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var param = new DynamicParameters();
                        param.Add("@Day", days);
                        param.Add("@BranchId", 0);
                        var data = db.Query<AbsentEmployees>(sql: "[dbo].[GetAbsentList]", param: param, commandType: CommandType.StoredProcedure);
                        foreach (var item in data)
                        {
                            if (!string.IsNullOrEmpty(item.photo))
                            {
                                var ImgPath = item.photo.Replace('\\', '/');
                                //item.photo = "www.onlinehajiri.com/" + ImgPath;
                                var request = HttpContext.Current.Request;
                                var appRootFolder = request.ApplicationPath;
                                if (!appRootFolder.EndsWith("/"))
                                {
                                    appRootFolder += "/";
                                }
                                var url = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appRootFolder);
                                item.photo = url + ImgPath;
                            }
                        }
                        return data;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<EarlyoutList> GetEarlyOutEmployess(int days, string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var param = new DynamicParameters();
                        param.Add("@NumberOfDays", days);
                        param.Add("@BranchId", 0);
                        var data = db.Query<EarlyoutList>(sql: "[dbo].[AttendanceDetails]", param: param, commandType: CommandType.StoredProcedure).Where(x => x.EarlyOut == 1);
                        return data;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<LateInList> GetLateInEmployess(int days, string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var param = new DynamicParameters();
                        param.Add("@NumberOfDays", days);
                        param.Add("@BranchId", 0);
                        var data = db.Query<LateInList>(sql: "[dbo].[AttendanceDetails]", param: param, commandType: CommandType.StoredProcedure).Where(x => x.LateIn == 1);
                        return data;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<LateInList> GetPresentEmployess(int days, string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                if (db.DataSource != "")
                {
                    try
                    {
                        db.Open();
                        var param = new DynamicParameters();
                        param.Add("@NumberOfDays", days);
                        param.Add("@BranchId", 0);
                        var data = db.Query<LateInList>(sql: "[dbo].[AttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);
                        return data;

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
        public IEnumerable<MonthlyReport> GetMonthlyReport(string ComCode, int Year, int Month, string DateType, int EmpId, string DeptId)
        {
            DateTime StartDate = DateTime.Now;
            DateTime EndDate = DateTime.Now;

            if (DateType == null || DateType == "English")
            {
                StartDate = new DateTime(Convert.ToInt32(Year), Month, 1);
                EndDate = new DateTime(Convert.ToInt32(Year), Month, DateTime.DaysInMonth(Convert.ToInt32(Year), Month));
            }
            else
            {
                StartDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, 1);
                int Day = NepDateConverter.DaysByYearMonth(Year, Month);
                EndDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, Day);
            }
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@StartDate", StartDate);
                    param.Add("@EndDate", EndDate);
                    List<MonthlyReport> MonthlyReport = new List<MonthlyReport>();

                    //db.Execute("[dbo].[DropProcedure]", commandType: CommandType.StoredProcedure);

                    //db.Execute("[dbo].[CreateProcedure]", commandType: CommandType.StoredProcedure);
                    if (Month == 1)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 2)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport1]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 3)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport2]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 4)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport3]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 5)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport4]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 6)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport5]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 7)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport6]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 8)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport7]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 9)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport8]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 10)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport9]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 11)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport10]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }
                    if (Month == 12)
                    {
                        MonthlyReport = db.Query<MonthlyReport>(sql: "[dbo].[GetMonthlyReport11]", param: param, commandType: CommandType.StoredProcedure).ToList();
                    }

                    int DepId = DeptId == "" ? 0 : Convert.ToInt32(DeptId);
                    if (MonthlyReport.Count() > 0)
                    {
                        if (EmpId > 0)
                        {
                            MonthlyReport = MonthlyReport.Where(x => x.EmpId == EmpId).ToList();
                        }
                        if (DepId > 0)
                        {
                            MonthlyReport = MonthlyReport.Where(x => x.Department_Id == DepId).ToList();
                        }
                    }

                    DateTime dateTime1 = DateTime.Now;
                    if (EndDate > dateTime1)
                    {
                        dateTime1 = DateTime.Now;
                    }
                    else
                    {
                        dateTime1 = EndDate;
                    }
                    var param1 = new DynamicParameters();
                    param1.Add("@StartDate", StartDate);
                    param1.Add("@EndDate", dateTime1);

                    var absCount = db.Query<MonthlyReport>(sql: "[dbo].[GetAbsentListCountForMonthly]", param: param1, commandType: CommandType.StoredProcedure);
                    var leaveCount = db.Query<MonthlyReport>(sql: "[dbo].[GetLeaveListCountForMonthly]", param: param, commandType: CommandType.StoredProcedure);
                    var earlyOutCount = db.Query<MonthlyReport>(sql: "[dbo].[GetEarlyOutCountForMonthly]", param: param, commandType: CommandType.StoredProcedure);
                    var LateInCount = db.Query<MonthlyReport>(sql: "[dbo].[GetLateInCountForMonthly]", param: param, commandType: CommandType.StoredProcedure);

                    var presentCount = db.Query<MonthlyReport>(sql: "[dbo].[MonthlyPresentReport]", param: param, commandType: CommandType.StoredProcedure);

                    List<MonthlyReport> mc = (from mR in MonthlyReport
                                              join pC in presentCount
                                              on mR.EmpId equals pC.EmpId into ps
                                              from p in ps.DefaultIfEmpty()
                                              join abs in absCount
                                              on mR.EmpId equals abs.EmpId into aps
                                              from a in aps.DefaultIfEmpty()
                                              join leave in leaveCount
                                              on mR.EmpId equals leave.EmpId into lv
                                              from l in lv.DefaultIfEmpty()
                                              join eO in earlyOutCount
                                              on mR.EmpId equals eO.EmpId into earlyOut
                                              from e in earlyOut.DefaultIfEmpty()
                                              join li in LateInCount
                                              on mR.EmpId equals li.EmpId into Lin
                                              from late in Lin.DefaultIfEmpty()
                                              select new MonthlyReport
                                              {

                                                  ShiftStart = mR.ShiftStart,
                                                  ShiftEnd = mR.ShiftEnd,
                                                  Emp_Name = mR.Emp_Name,
                                                  EmpId = mR.EmpId,
                                                  Religion = mR.Religion,
                                                  Department_Id = mR.Department_Id,
                                                  Gender = mR.Gender,
                                                  WeeklyOff = mR.WeeklyOff,
                                                  LogInTime_1 = mR.LogInTime_1,
                                                  LogOutTime_1 = mR.LogOutTime_1,
                                                  LogInTime_2 = mR.LogInTime_2,
                                                  LogOutTime_2 = mR.LogOutTime_2,
                                                  LogInTime_3 = mR.LogInTime_3,
                                                  LogOutTime_3 = mR.LogOutTime_3,
                                                  LogInTime_4 = mR.LogInTime_4,
                                                  LogOutTime_4 = mR.LogOutTime_4,
                                                  LogInTime_5 = mR.LogInTime_5,
                                                  LogOutTime_5 = mR.LogOutTime_5,
                                                  LogInTime_6 = mR.LogInTime_6,
                                                  LogOutTime_6 = mR.LogOutTime_6,
                                                  LogInTime_7 = mR.LogInTime_7,
                                                  LogOutTime_7 = mR.LogOutTime_7,
                                                  LogInTime_8 = mR.LogInTime_8,
                                                  LogOutTime_8 = mR.LogOutTime_8,
                                                  LogInTime_9 = mR.LogInTime_9,
                                                  LogOutTime_9 = mR.LogOutTime_9,
                                                  LogInTime_10 = mR.LogInTime_10,
                                                  LogOutTime_10 = mR.LogOutTime_10,
                                                  LogInTime_11 = mR.LogInTime_11,
                                                  LogOutTime_11 = mR.LogOutTime_11,
                                                  LogInTime_12 = mR.LogInTime_12,
                                                  LogOutTime_12 = mR.LogOutTime_12,
                                                  LogInTime_13 = mR.LogInTime_13,
                                                  LogOutTime_13 = mR.LogOutTime_13,
                                                  LogInTime_14 = mR.LogInTime_14,
                                                  LogOutTime_14 = mR.LogOutTime_14,
                                                  LogInTime_15 = mR.LogInTime_15,
                                                  LogOutTime_15 = mR.LogOutTime_15,
                                                  LogInTime_16 = mR.LogInTime_16,
                                                  LogOutTime_16 = mR.LogOutTime_16,
                                                  LogInTime_17 = mR.LogInTime_17,
                                                  LogOutTime_17 = mR.LogOutTime_17,
                                                  LogInTime_18 = mR.LogInTime_18,
                                                  LogOutTime_18 = mR.LogOutTime_18,
                                                  LogInTime_19 = mR.LogInTime_19,
                                                  LogOutTime_19 = mR.LogOutTime_19,
                                                  LogInTime_20 = mR.LogInTime_20,
                                                  LogOutTime_20 = mR.LogOutTime_20,
                                                  LogInTime_21 = mR.LogInTime_21,
                                                  LogOutTime_21 = mR.LogOutTime_21,
                                                  LogInTime_22 = mR.LogInTime_22,
                                                  LogOutTime_22 = mR.LogOutTime_22,
                                                  LogInTime_23 = mR.LogInTime_23,
                                                  LogOutTime_23 = mR.LogOutTime_23,
                                                  LogInTime_24 = mR.LogInTime_24,
                                                  LogOutTime_24 = mR.LogOutTime_24,
                                                  LogInTime_25 = mR.LogInTime_25,
                                                  LogOutTime_25 = mR.LogOutTime_25,
                                                  LogInTime_26 = mR.LogInTime_26,
                                                  LogOutTime_26 = mR.LogOutTime_26,
                                                  LogInTime_27 = mR.LogInTime_27,
                                                  LogOutTime_27 = mR.LogOutTime_27,
                                                  LogInTime_28 = mR.LogInTime_28,
                                                  LogOutTime_28 = mR.LogOutTime_28,
                                                  LogInTime_29 = mR.LogInTime_29,
                                                  LogOutTime_29 = mR.LogOutTime_29,
                                                  LogInTime_30 = mR.LogInTime_30,
                                                  LogOutTime_30 = mR.LogOutTime_30,
                                                  LogInTime_31 = mR.LogInTime_31,
                                                  LogOutTime_31 = mR.LogOutTime_31,
                                                  LogInTime_32 = mR.LogInTime_32,
                                                  LogOutTime_32 = mR.LogOutTime_32,

                                                  LateIn1 = mR.LateIn1,
                                                  EarlyOut1 = mR.EarlyOut1,
                                                  LateIn2 = mR.LateIn2,
                                                  EarlyOut2 = mR.EarlyOut2,
                                                  LateIn3 = mR.LateIn3,
                                                  EarlyOut3 = mR.EarlyOut3,
                                                  LateIn4 = mR.LateIn4,
                                                  EarlyOut4 = mR.EarlyOut4,
                                                  LateIn5 = mR.LateIn5,
                                                  EarlyOut5 = mR.EarlyOut5,
                                                  LateIn6 = mR.LateIn6,
                                                  EarlyOut6 = mR.EarlyOut6,
                                                  LateIn7 = mR.LateIn7,
                                                  EarlyOut7 = mR.EarlyOut7,
                                                  LateIn8 = mR.LateIn8,
                                                  EarlyOut8 = mR.EarlyOut8,
                                                  LateIn9 = mR.LateIn9,
                                                  EarlyOut9 = mR.EarlyOut9,
                                                  LateIn10 = mR.LateIn10,
                                                  EarlyOut10 = mR.EarlyOut10,
                                                  LateIn11 = mR.LateIn11,
                                                  EarlyOut11 = mR.EarlyOut11,
                                                  LateIn12 = mR.LateIn12,
                                                  EarlyOut12 = mR.EarlyOut12,
                                                  LateIn13 = mR.LateIn13,
                                                  EarlyOut13 = mR.EarlyOut13,
                                                  LateIn14 = mR.LateIn14,
                                                  EarlyOut14 = mR.EarlyOut14,
                                                  LateIn15 = mR.LateIn15,
                                                  EarlyOut15 = mR.EarlyOut15,
                                                  LateIn16 = mR.LateIn16,
                                                  EarlyOut16 = mR.EarlyOut16,
                                                  LateIn17 = mR.LateIn17,
                                                  EarlyOut17 = mR.EarlyOut17,
                                                  LateIn18 = mR.LateIn18,
                                                  EarlyOut18 = mR.EarlyOut18,
                                                  LateIn19 = mR.LateIn19,
                                                  EarlyOut19 = mR.EarlyOut19,
                                                  LateIn20 = mR.LateIn20,
                                                  EarlyOut20 = mR.EarlyOut20,
                                                  LateIn21 = mR.LateIn21,
                                                  EarlyOut21 = mR.EarlyOut21,
                                                  LateIn22 = mR.LateIn22,
                                                  EarlyOut22 = mR.EarlyOut22,
                                                  LateIn23 = mR.LateIn23,
                                                  EarlyOut23 = mR.EarlyOut23,
                                                  LateIn24 = mR.LateIn24,
                                                  EarlyOut24 = mR.EarlyOut24,
                                                  LateIn25 = mR.LateIn25,
                                                  EarlyOut25 = mR.EarlyOut25,
                                                  LateIn26 = mR.LateIn26,
                                                  EarlyOut26 = mR.EarlyOut26,
                                                  LateIn27 = mR.LateIn27,
                                                  EarlyOut27 = mR.EarlyOut27,
                                                  LateIn28 = mR.LateIn28,
                                                  EarlyOut28 = mR.EarlyOut28,
                                                  LateIn29 = mR.LateIn29,
                                                  EarlyOut29 = mR.EarlyOut29,
                                                  LateIn30 = mR.LateIn30,
                                                  EarlyOut30 = mR.EarlyOut30,
                                                  LateIn31 = mR.LateIn31,
                                                  EarlyOut31 = mR.EarlyOut31,
                                                  LateIn32 = mR.LateIn32,
                                                  EarlyOut32 = mR.EarlyOut32,
                                                  LateInCount = late == null ? 0 : late.LateInCount,
                                                  EarlyOutCount = e == null ? 0 : e.EarlyOutCount,
                                                  LeaveCount = l == null ? 0 : l.LeaveCount,
                                                  AbsentCount = a == null ? 0 : a.AbsentCount,
                                                  PresentCount = p == null ? 0 : p.PresentCount,
                                                  LEA = mR.LEA

                                              }).ToList();

                    db.Close();
                    return mc;
                }
            }
            catch (Exception)
            {
                return null;
            }


        }
        public IEnumerable<Holiday> GetHolidays(int Year, int Month, string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {

                    var DateType = HttpContext.Current.Session["DatePicker"].ToString();
                    DateTime StartDate = DateTime.Now;
                    DateTime EndDate = DateTime.Now;

                    if (DateType == null || DateType == "English")
                    {
                        StartDate = new DateTime(Convert.ToInt32(Year), Month, 1);
                        EndDate = new DateTime(Convert.ToInt32(Year), Month, DateTime.DaysInMonth(Convert.ToInt32(Year), Month));
                    }
                    else
                    {
                        StartDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, 1);
                        int Day = NepDateConverter.DaysByYearMonth(Year, Month);
                        EndDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), Month, Day);
                    }

                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@FromDate", StartDate);
                    param.Add("@EndDate", EndDate);

                    return db.Query<Holiday>(sql: "[dbo].[GetHolidayByDate]", param: param, commandType: CommandType.StoredProcedure).ToList();

                }

            }
            catch (Exception)
            {
                return null;
            }
        }
        public IEnumerable<FilteredLeave> GetLeaveDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate)
        {
            if (empId == "0")
            {
                empId = "";
            }
            if (deptId == "0")
            {
                deptId = "";
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    var leaveReport = db.Query<FilteredLeave>(sql: "[dbo].[GetOverallLeave]", param: param, commandType: CommandType.StoredProcedure);
                    if (empId != "")
                    {
                        leaveReport = leaveReport.Where(x => x.EmpId == Convert.ToInt32(empId));
                    }
                    if (deptId != "")
                    {
                        leaveReport = leaveReport.Where(x => x.DeptId == Convert.ToInt32(deptId));
                    }
                    return leaveReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public IEnumerable<HolidayDetails> GetHolidayDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate)
        {
            if (empId == "0")
            {
                empId = "";
            }
            if (deptId == "0")
            {
                deptId = "";
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    var WeekLyHolidayReport = db.Query<HolidayDetails>(sql: "[dbo].[GetOverallHolidayDetails]", param: param, commandType: CommandType.StoredProcedure);
                    if (empId != "")
                    {
                        WeekLyHolidayReport = WeekLyHolidayReport.Where(x => x.EmpId == Convert.ToInt32(empId));
                    }
                    if (deptId != "")
                    {
                        WeekLyHolidayReport = WeekLyHolidayReport.Where(x => x.DeptId == Convert.ToInt32(deptId));
                    }
                    return WeekLyHolidayReport.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }
        public List<LeaveDetails> LeaveDetails(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                db.Open();
                var data = db.Query<LeaveDetails>("select Code, FromDateNepali, ToDateNepali,FromDate as LeaveFromDate,ToDate as LeaveToDate,LeaveName,EmployeeId, LeaveDay from LeaveApplication as LA inner join LeaveMaster as LM on LA.LeaveId = LM.Id where LA.Status='A'", commandType: CommandType.Text);
                return data.ToList();
            }
        }
        public Color GetColor(string ComCode)
        {
            var Color = new Color();
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                var param = new DynamicParameters();
                db.Open();

                Color = db.Query<Color>("SELECT TOP 1 * from Color", commandType: CommandType.Text).SingleOrDefault();

                if (Color == null)
                {
                    Color color = new Color();
                    color.LateInColor = "#e06666";
                    color.EarlyOutColor = "ea9999";
                    color.WeekendColor = "#98d0dd";
                    color.AbsentColor = "#8c0404";
                    color.HolidayColor = "#eeeeee";
                    color.ShiftNotAsignColor = "#ff0000";
                    color.AbsentByLE = "#f50057";
                    color.DidNotLogoutColor = "#eeeeee";
                    color.LeaveColor = "#cc0000";
                    color.PresentColor = "rgb(84, 142, 48)";
                    return color;
                }
                else
                {
                    return Color;
                }
            }
        }

        public List<LeaveDetails> LeaveCodeList(string ComCode)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                var str = "";
                db.Open();
                var data = db.Query<LeaveDetails>("SELECT CODE,LeaveName FROM LeaveMaster WHERE IsDeleted= 0", commandType: CommandType.Text);

                return data.ToList();
            }
        }

        public IEnumerable<Attendance> GetDailyReport(string ComCode, string Date, string DateType, string EmpId, string DeptId, string DesignationId, string DeviceCode)
        {
            try
            {
                if (DateType == null || DateType == "Nepali")
                {
                    string[] date = Date.Split('-');
                    Date = NepDateConverter.NepToEng(Convert.ToInt32(date[0]), Convert.ToInt32(date[1]), Convert.ToInt32(date[2])).ToString();
                }
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Date", Date);
                    param.Add("@EmpId", EmpId);
                    param.Add("@DeptId", DeptId);
                    param.Add("@DesignationId", DesignationId);
                    param.Add("@DeviceCode", DeviceCode);
                    var Attendancelist = db.Query<Attendance>(sql: "[dbo].[GetAttendanceList]", param: param, commandType: CommandType.StoredProcedure);
                    return Attendancelist.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<YearlyAttendance> GetAllYearlyData(string ComCode, string Year, string DateType, string EmpId, string designationId, string departmentId)
        {
            try
            {
                DateTime StartDate1 = new DateTime(Convert.ToInt32(Year), 1, 1);
                DateTime EndDate1 = new DateTime(Convert.ToInt32(Year), 1, DateTime.DaysInMonth(Convert.ToInt32(Year), 1));
                DateTime StartDate2 = new DateTime(Convert.ToInt32(Year), 2, 1);
                DateTime EndDate2 = new DateTime(Convert.ToInt32(Year), 2, DateTime.DaysInMonth(Convert.ToInt32(Year), 2));
                DateTime StartDate3 = new DateTime(Convert.ToInt32(Year), 3, 1);
                DateTime EndDate3 = new DateTime(Convert.ToInt32(Year), 3, DateTime.DaysInMonth(Convert.ToInt32(Year), 3));
                DateTime StartDate4 = new DateTime(Convert.ToInt32(Year), 4, 1);
                DateTime EndDate4 = new DateTime(Convert.ToInt32(Year), 4, DateTime.DaysInMonth(Convert.ToInt32(Year), 4));
                DateTime StartDate5 = new DateTime(Convert.ToInt32(Year), 5, 1);
                DateTime EndDate5 = new DateTime(Convert.ToInt32(Year), 5, DateTime.DaysInMonth(Convert.ToInt32(Year), 5));
                DateTime StartDate6 = new DateTime(Convert.ToInt32(Year), 6, 1);
                DateTime EndDate6 = new DateTime(Convert.ToInt32(Year), 6, DateTime.DaysInMonth(Convert.ToInt32(Year), 6));
                DateTime StartDate7 = new DateTime(Convert.ToInt32(Year), 7, 1);
                DateTime EndDate7 = new DateTime(Convert.ToInt32(Year), 7, DateTime.DaysInMonth(Convert.ToInt32(Year), 7));
                DateTime StartDate8 = new DateTime(Convert.ToInt32(Year), 8, 1);
                DateTime EndDate8 = new DateTime(Convert.ToInt32(Year), 8, DateTime.DaysInMonth(Convert.ToInt32(Year), 8));
                DateTime StartDate9 = new DateTime(Convert.ToInt32(Year), 9, 1);
                DateTime EndDate9 = new DateTime(Convert.ToInt32(Year), 9, DateTime.DaysInMonth(Convert.ToInt32(Year), 9));
                DateTime StartDate10 = new DateTime(Convert.ToInt32(Year), 10, 1);
                DateTime EndDate10 = new DateTime(Convert.ToInt32(Year), 10, DateTime.DaysInMonth(Convert.ToInt32(Year), 10));
                DateTime StartDate11 = new DateTime(Convert.ToInt32(Year), 11, 1);
                DateTime EndDate11 = new DateTime(Convert.ToInt32(Year), 11, DateTime.DaysInMonth(Convert.ToInt32(Year), 11));
                DateTime StartDate12 = new DateTime(Convert.ToInt32(Year), 12, 1);
                DateTime EndDate12 = new DateTime(Convert.ToInt32(Year), 12, DateTime.DaysInMonth(Convert.ToInt32(Year), 12));

                if (DateType == "Nepali")
                {
                    StartDate1 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 1, 1);
                    int Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 1);
                    EndDate1 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 1, Day);

                    StartDate2 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 2, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 2);
                    EndDate2 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 2, Day);

                    StartDate3 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 3, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 3);
                    EndDate3 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 3, Day);

                    StartDate4 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 4, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 4);
                    EndDate4 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 4, Day);

                    StartDate5 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 5, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 5);
                    EndDate5 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 5, Day);

                    StartDate6 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 6, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 6);
                    EndDate6 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 6, Day);

                    StartDate7 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 7, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 7);
                    EndDate7 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 7, Day);

                    StartDate8 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 8, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 8);
                    EndDate8 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 8, Day);

                    StartDate9 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 9, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 9);
                    EndDate9 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 9, Day);

                    StartDate10 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 10, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 10);
                    EndDate10 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 10, Day);

                    StartDate11 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 11, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 11);
                    EndDate11 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 11, Day);

                    StartDate12 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 12, 1);
                    Day = NepDateConverter.DaysByYearMonth(Convert.ToInt32(Year), 12);
                    EndDate12 = NepDateConverter.NepToEng(Convert.ToInt32(Year), 12, Day);
                }

                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@Year", Year);
                    param.Add("@EmpId", EmpId);
                    param.Add("@StartDate1", StartDate1);
                    param.Add("@EndDate1", EndDate1);
                    param.Add("@StartDate2", StartDate2);
                    param.Add("@EndDate2", EndDate2);
                    param.Add("@StartDate3", StartDate3);
                    param.Add("@EndDate3", EndDate3);
                    param.Add("@StartDate4", StartDate4);
                    param.Add("@EndDate4", EndDate4);
                    param.Add("@StartDate5", StartDate5);
                    param.Add("@EndDate5", EndDate5);
                    param.Add("@StartDate6", StartDate6);
                    param.Add("@EndDate6", EndDate6);
                    param.Add("@StartDate7", StartDate7);
                    param.Add("@EndDate7", EndDate7);
                    param.Add("@StartDate8", StartDate8);
                    param.Add("@EndDate8", EndDate8);
                    param.Add("@StartDate9", StartDate9);
                    param.Add("@EndDate9", EndDate9);
                    param.Add("@StartDate10", StartDate10);
                    param.Add("@EndDate10", EndDate10);
                    param.Add("@StartDate11", StartDate11);
                    param.Add("@EndDate11", EndDate11);
                    param.Add("@StartDate12", StartDate12);
                    param.Add("@EndDate12", EndDate12);
                    param.Add("@DeviceCode", 0);
                    param.Add("@DesignationId", designationId);
                    param.Add("@DepartmentId", departmentId);
                    var Attendancelist = db.Query<YearlyAttendance>(sql: "[dbo].[GetYearlyAttendance]", param: param, commandType: CommandType.StoredProcedure);
                    foreach (var item in Attendancelist)
                    {
                        item.Year = Year;
                    }
                    Attendancelist = Attendancelist.Where(x => x.Emp_Name != null);
                    Attendancelist = Attendancelist.Where(x => x.Year == Year);
                    return Attendancelist.ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeeSummary> GetEmpSummaryDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@StartDate", stDate);
                    param.Add("@EndDate", endDate);
                    param.Add("@empId", empId);
                    param.Add("@deptId", deptId);
                    var empSummary = db.Query<EmployeeSummary>(sql: "[dbo].[GetEmpSummaryReport]", param: param, commandType: CommandType.StoredProcedure);

                    return empSummary.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        public IEnumerable<AttendanceDetails> GetAttendanceDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    param.Add("@empId", empId);
                    param.Add("@deptId", deptId);
                    var data = db.Query<AttendanceDetails>(sql: "[dbo].[GetOverallAttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);

                    //if (getReportFor == "0")
                    //{
                    //    WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.LateIn == 1);
                    //}
                    //if (getReportFor == "1")
                    //{
                    //    WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EarlyOut == 1);
                    //}
                    //if (getReportFor == "2")
                    //{
                    //    WeekLyAttendanceReport = WeekLyAttendanceReport.Where(x => x.EarlyOutAbsent == 1 || x.LateInAbsent == 1 || x.Status == "Absent");
                    //}

                    return data.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public IEnumerable<AttendanceDetails> DailyPresent(string ComCode)
        {
            try
            {
                var dbfactory = DbFactoryProvider.GetFactory();
                using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                {
                    if (db.DataSource != "")
                    {
                        var param = new DynamicParameters();
                        db.Open();
                        param.Add("@NumberOfDays", 0);
                        var data = db.Query<AttendanceDetails>(sql: "[dbo].[AttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);
                        return data;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<AttendanceDetails> GetEmployeeAttendanceDetails(string ComCode, string empId, DateTime stDate, DateTime endDate)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    param.Add("@empId", empId);
                    param.Add("@deptId", 0);
                    var AttReport = db.Query<AttendanceDetails>(sql: "[dbo].[GetOverallAttendanceDetails]", param: param, commandType: CommandType.StoredProcedure);
                    return AttReport.ToList();
                }
                catch (Exception ex)
                {
                    throw null;
                }


            }
        }

        public IEnumerable<FilteredLeave> GetLeaveDetails(string ComCode, string empId, DateTime stDate, DateTime endDate)
        {
            if (empId == "0")
            {
                empId = "";
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    var leaveReport = db.Query<FilteredLeave>(sql: "[dbo].[GetOverallLeave]", param: param, commandType: CommandType.StoredProcedure);
                    if (empId != "")
                    {
                        leaveReport = leaveReport.Where(x => x.EmpId == Convert.ToInt32(empId));
                    }
                    return leaveReport.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public IEnumerable<HolidayDetails> GetHolidayDetails(string ComCode, string empId, DateTime stDate, DateTime endDate)
        {
            if (empId == "0")
            {
                empId = "";
            }
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var param = new DynamicParameters();
                    param.Add("@startDate", stDate);
                    param.Add("@endDate", endDate);
                    var WeekLyHolidayReport = db.Query<HolidayDetails>(sql: "[dbo].[GetOverallHolidayDetails]", param: param, commandType: CommandType.StoredProcedure);
                    if (empId != "")
                    {
                        WeekLyHolidayReport = WeekLyHolidayReport.Where(x => x.EmpId == Convert.ToInt32(empId));
                    }
                    return WeekLyHolidayReport.ToList();

                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public string RemoteAttendance(string ComCode, string empId, string DeviceCode, string DateTime, string Latitude, string Longitude)
        {
            var res = CheckAttendanceParameter(ComCode, empId, DateTime, Latitude, Longitude);
            if (res == "YouCanDoAttendance")
            {
                try
                {
                    var dbfactory = DbFactoryProvider.GetFactory();
                    using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
                    {
                        db.Open();
                        var param = new DynamicParameters();
                        param.Add("@Id", 0);
                        param.Add("@DlId", 0);
                        param.Add("@Event", 'I');
                        param.Add("@LogInTime", Convert.ToDateTime(DateTime));
                        param.Add("@logoutTime", Convert.ToDateTime(DateTime));
                        param.Add("@CreatedDate", Convert.ToDateTime(DateTime));
                        param.Add("@EmpAId", Convert.ToInt32(DeviceCode));
                        param.Add("@CreatedBy", Convert.ToInt32(empId));
                        param.Add("@AttendanceType", 'R');
                        param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);

                        db.Execute("[dbo].[Usp_IUD_ManualAttendanceLog]", param: param, commandType: CommandType.StoredProcedure);
                        db.Close();

                        var data = param.Get<Int16>("@Return_Id");
                        if(data > 0)
                        {
                            return "Attendance Success";
                        }
                        else
                        {
                            return "Attendance Failed";
                        }

                    }
                }
                catch (Exception ex)
                {
                    return "Attendance Failed";
                }
            }
            else
            {
                return res;
            }
            
        }
        public string CheckAttendanceParameter(string ComCode, string empId, string DateTime, string Latitude, string Longitude)
        {
            var dbfactory = DbFactoryProvider.GetFactory();
            using (var db = (DbConnection)dbfactory.GetConnection(ComCode))
            {
                try
                {
                    db.Open();
                    var dateeng = Convert.ToDateTime(DateTime).Date;
                    var ap = db.Query<RemoteAttendance>(sql: "Select * from RemoteAttendance where EmployeeId = @empId and DateEng = @dateEng", param: new { @empId = empId, @dateEng = dateeng }, commandType: CommandType.Text).SingleOrDefault();
                    if (ap == null)
                    {
                        return "Your Remote attendace is not set today. Please contact Your administrator!!";
                    }
                    else
                    {
                        var dis = distance(Convert.ToDouble(Latitude), Convert.ToDouble(Longitude), Convert.ToDouble(ap.Latitude), Convert.ToDouble(ap.Longitude), 'M');
                        if (dis <= ap.Distance)
                        {
                            return "YouCanDoAttendance";
                        }
                        else
                        {
                            return "You are out of your attendance range";
                        }
                    }
                }
                catch (Exception ex)
                {
                    return "Attendance Failed";
                }
            }
        }

        private double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                if (unit == 'K')
                {
                    dist = dist * 1.609344;
                }
                else if (unit == 'N')
                {
                    dist = dist * 0.8684;
                }
                return (dist);
            }
        }
        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

    }
}