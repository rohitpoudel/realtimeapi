﻿using Entity;
using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryProcessor
{
    public interface IAttendanceQueryProcessor
    {
        IEnumerable<LateInList> GetLateInEmployess(int days, string ComCode);
        IEnumerable<EarlyoutList> GetEarlyOutEmployess(int days, string ComCode);
        IEnumerable<AbsentEmployees> GetAbsentEmployess(int days, string ComCode);
        IEnumerable<LateInList> GetPresentEmployess(int days, string ComCode);
        IEnumerable<MonthlyReport> GetMonthlyReport(string ComCode, int Year, int Month, string DateType, int EmpId, string DeptId);
        IEnumerable<Holiday> GetHolidays(int Year, int Month, string ComCode);
        List<LeaveDetails> LeaveDetails(string ComCode);
        Color GetColor(string ComCode);
        List<LeaveDetails> LeaveCodeList(string ComCode);

        IEnumerable<HolidayDetails> GetHolidayDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate);
        IEnumerable<FilteredLeave> GetLeaveDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate);

        //For Daily Attendance Report. 
        IEnumerable<Attendance> GetDailyReport(string ComCode, string Date, string DateType, string EmpId, string DeptId, string DesignationId, string DeviceCode);

        //For Yearly Attendance Report
        IEnumerable<YearlyAttendance> GetAllYearlyData(string ComCode, string Year, string DateType, string EmpId, string designationId, string departmentId);

        // For Employee Summary Report
        IEnumerable<EmployeeSummary> GetEmpSummaryDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate);

        // For Employee Detailed Report
        IEnumerable<AttendanceDetails> GetAttendanceDetails(string ComCode, string empId, string deptId, DateTime stDate, DateTime endDate);

        IEnumerable<AttendanceDetails> DailyPresent(string ComCode);

        IEnumerable<AttendanceDetails> GetEmployeeAttendanceDetails(string ComCode, string empId, DateTime stDate, DateTime endDate);
        IEnumerable<HolidayDetails> GetHolidayDetails(string ComCode, string empId, DateTime stDate, DateTime endDate);
        IEnumerable<FilteredLeave> GetLeaveDetails(string ComCode, string empId, DateTime stDate, DateTime endDate);

        string RemoteAttendance(string ComCode, string empId, string DeviceCode, string DateTime, string Latitude, string Longitude);

    }
}
