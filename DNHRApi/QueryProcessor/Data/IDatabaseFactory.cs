using System;
using System.Data;

namespace QueryProcessor
{
    public interface IDatabaseFactory : IDisposable
    {
        IDbConnection Db { get; }
        Dialect Dialect { get; }
        QueryBuilder QueryBuilder { get; }
        IDbConnection GetConnection(string CompanyCode);
       
     }
}