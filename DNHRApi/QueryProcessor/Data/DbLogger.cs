using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace QueryProcessor
{
    public class DefaultDbLoggerSetting 
    {
        public bool AllowLogging { get; set; } = false;
    }

  
}