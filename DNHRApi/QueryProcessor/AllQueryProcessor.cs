﻿using QueryProcessor.DashboardQueryProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QueryProcessor
{
    public class AllQueryProcessor
    {
        public IDashboardProcessor DashboardProcessor = new DashboardProcessor();
        public ILoginProcessor loginprocessor = new LoginProcessor();
        public IEmployeeQueryProcessor EmployeeQueryProcessor = new EmployeeQueryProcessor();
        public IAttendanceQueryProcessor AttendanceQueryProcessor = new AttendanceQueryProcessor();
        public ICompanyQueryProcessor CompanyQueryProcessor = new CompanyQueryProcessor();
    }
}