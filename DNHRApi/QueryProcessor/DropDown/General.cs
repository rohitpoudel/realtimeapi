﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using QueryProcessor;
using System.Configuration;
using System.Web;
using Dapper;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace QueryProcessor.DropDown
{
    public static class General
    {

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "DigitalNepal";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "DigitalNepal";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static DataTable ConvertToDataTable<T>(this IEnumerable<T> data)
        {
            List<T> list = data.Cast<T>().ToList();

            PropertyDescriptorCollection props = null;
            DataTable table = new DataTable();
            if (list != null && list.Count > 0)
            {
                props = TypeDescriptor.GetProperties(list[0]);
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            if (props != null)
            {
                object[] values = new object[props.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            return table;
        }
        public static string ConvertDateFormat(string Date)
        {
            string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
            if (Date.Length > 10)
            {
                Date = Date.Substring(0, Date.Length - 11);
            }
            string[] date = Date.Split('/').ToArray();

          
            if (sysFormat == "M/d/yyyy")
            {
                Date = date[2] + "-" + date[0] + "-" + date[1];
            }
            else
            {
                Date = date[2] + "-" + date[1] + "-" + date[0];
            }




            return Date;
        }
        public static string DifferenceInHourMins(string Time1, string Time2)

        {
            if (string.IsNullOrEmpty(Time1))
            {
                Time1 = "00:00";
            }
            if (string.IsNullOrEmpty(Time2))
            {
                Time2 = "00:00";
            }
            TimeSpan t1 = TimeSpan.Parse(Time1);
            TimeSpan t2 = TimeSpan.Parse(Time2);
            t1.Subtract(t2).ToString();

            return t1.Subtract(t2).ToString();
        }
        public static SqlConnectionStringBuilder BuildDyanmicString(string CompanyCode)
        {
            ICompanyQueryProcessor companyQueryProcessor = new CompanyQueryProcessor();

            var data = companyQueryProcessor.GetByCode(CompanyCode);
            if (data.DataSource != null)
            {
                SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
                connectionString.DataSource = data.DataSource;
                connectionString.InitialCatalog = data.DB_Name;
                connectionString.UserID = data.Db_UserName;
                connectionString.Password = Decrypt(data.Password);
                connectionString.IntegratedSecurity = false;
                return connectionString;
            }
            else
            {
                SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
                return connectionString;
            }
        }
        public static bool CheckDatabaseExists(string connectionString, string databaseName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand($"SELECT db_id('{databaseName}')", connection))
                {
                    bool result = false;
                    connection.Open();
                    result = command.ExecuteScalar() != DBNull.Value;
                    connection.Close();
                    return result;
                }
            }
        }
        public static int DataBaseCreation(string DbName,string CompanyCode,string companyname)
        {
        
            var filePath = HttpContext.Current.Server.MapPath("~/DBScript/db.sql");
            bool isFileExist = File.Exists(filePath);
            if (isFileExist == true)
            {
                FileInfo file = new FileInfo(filePath);
                string script = file.OpenText().ReadToEnd();
                if (script != "")
                {
                    string sqlConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                    SqlConnection conn = new SqlConnection(sqlConnectionString);
                    Server server = new Server(new ServerConnection(conn));
                    var dbName = DbName;
                    var text = script.Split(' ');
                    bool isDbExists = CheckDatabaseExists(sqlConnectionString, dbName);
                    if (isDbExists == false)
                    {
                        try
                        {
                            // database creation part
                            conn.Open();
                            server.ConnectionContext.ExecuteNonQuery(string.Format("CREATE DATABASE [{0}]", DbName));
                            conn.Close();

                            //run scrip part
                            string connection = BuildDyanmicString(CompanyCode).ToString();
                            SqlConnection dconn = new SqlConnection(connection);
                            Server servers = new Server(new ServerConnection(dconn));
                            dconn.Open();
                            servers.ConnectionContext.ExecuteNonQuery(script);
                            dconn.Close();
                            //insert into company table
                            dconn.Open();
                            string compquery = @"INSERT INTO [dbo].[Companies]
                                           ([CompanyName]
                                           ,[CompanyCode]
                                           ,[PhoneNo]
                                           ,[Email]
                                           ,[Web]
                                           ,[Logo]
                                           ,[CompanyAddress]
                                           ,[IsDeleted]
                                           ,[CreatedBy]
                                           ,[CreatedDate])
                                    Select CompanyName,CompanyCode,PhoneNo,Email,Web,Logo,CompanyAddress,IsDeleted,CreatedBy,CreatedDate from HRM.dbo.Companies where CompanyCode='" + CompanyCode + "'";
                            servers.ConnectionContext.ExecuteNonQuery(compquery);
                            dconn.Close();
                            ///insert into application user
                            ///
                            var dbfactory = DbFactoryProvider.GetFactory();
                            dconn.Open();
                            var param = new DynamicParameters();
                            param.Add("@Id", 0);
                            param.Add("@Event", 'I');
                            param.Add("@RoleId", 1);
                            param.Add("@UserId", 1);
                            param.Add("@Password", "AS1cBu3+GcFiO3ZVwmHN7A==");
                            param.Add("@Company_Code", string.Format("{0}-1", CompanyCode));
                            param.Add("@UserName", string.Format("{0}@admin.com", companyname));
                            param.Add("@BranchId", 1);
                            param.Add("@CreatedBy", 1);
                            param.Add("@CreatedDate", General.ConvertDateFormat(DateTime.Today.ToString()));
                            param.Add("@IsActive", true);
                            param.Add("@Return_Id", dbType: DbType.Int16, direction: ParameterDirection.Output);
                            dconn.Execute("[dbo].[Usp_IUD_ApplicationUser]", param: param, commandType: CommandType.StoredProcedure);
                            dconn.Close();
                            int i = param.Get<Int16>("@Return_Id");

                            /// Employee Insert
                            dconn.Open();
                            string empquery = @"INSERT INTO [dbo].[Employee]
                                       ([Emp_Code]
                                       ,[Emp_Name]
                                       ,[Emp_DeviceCode]
                                       ,[Designation_Id]
                                       ,[Department_Id]
                                       ,[Section_Id]
                                       ,[GradeGroup]        
                                       ,[Email]          
                                       ,[Is_Active]
                                       ,[Created_Date]
                                       ,[Created_By])
                                 VALUES('001','" + string.Format("{0}@admin.com", companyname) + "',0,1,1,1,1,'" + string.Format("{0}@admin.com", companyname) + "',1,GETDATE(),1)";
                            servers.ConnectionContext.ExecuteNonQuery(empquery);
                            dconn.Close();




                        }
                        catch (Exception ex)
                        {

                            Database db = new Database(server, dbName);
                            db.Refresh();
                            db.Drop();
                            throw ex;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                
                }

            }
            return 1;
        }
    }
}