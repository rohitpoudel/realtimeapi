﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace DNHR.API
{
    #region Authorize Attribute


    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var xyz = HttpContext.Current.Session["UserId"];
            HttpContext context = System.Web.HttpContext.Current;
            // var userId = Session["UserId"];
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            if (HttpContext.Current.Session["Token"] == null)
            {
                var routeValues = new RouteValueDictionary(new
                {
                    controller = "Login",
                    action = "Login",
                });
                filterContext.Result = new RedirectToRouteResult(routeValues);
             
            }
            else
            {
                System.Web.HttpContext.Current.Response.ClearHeaders();
                System.Web.HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
            }
           
        }
    
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            var url = filterContext.HttpContext.Request.Url;

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        Error = "NotAuthorized",
                        LogOnUrl = FormsAuthentication.LoginUrl
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "home" }, { "action", "index" } });

                // filterContext.HttpContext.Response.End();
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

    }

    #endregion

}