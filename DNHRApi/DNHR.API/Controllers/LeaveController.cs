﻿using Entity.Model;
using QueryProcessor;
using QueryProcessor.QueryProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DNHR.API.Controllers
{
    public class LeaveController : BaseController
    {
        LeaveApplicationQueryProcessor QueryProcessor = new LeaveApplicationQueryProcessor();
        CompanyQueryProcessor CompanyQueryProcessor = new CompanyQueryProcessor();
        public JsonResult EmployeeLeavelist(string EmpId,string ComCode)
        {
            var results = "Cannot get data this Time";

            var data = QueryProcessor.GetAllData( ComCode);
            string rolename = CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(EmpId), ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmployeeId == Convert.ToInt32(EmpId));
            }
          
            if (data.Count()> 0)
            {
                results = "Successfully";
            }
            var result = new { success = true, body = data, message = results };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeLeaveReport(string EmpId, string ComCode)
        {
            var results = "Cannot get data this Time";

            var data = QueryProcessor.GetLeaveReport(0, 0, 0, 'n',ComCode);
            string rolename = CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(EmpId), ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(EmpId));
            }
           
            if (data.Count() > 0)
            {
                results = "Successfully";
            }
            var result = new { success = true, body = data, message = results };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeDetailLeaveReport(int EmpId, int LeaveAssignId, string ComCode)
        {
            var results = "Cannot get data this Time";

            var data = QueryProcessor.GetDetailLeaveReport(EmpId, LeaveAssignId, ComCode);
            string rolename = CompanyQueryProcessor.GetRoleNameByEmpId(Convert.ToInt32(EmpId), ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(EmpId));
            }
          
            if (data.Count() > 0)
            {
                results = "Successfully";
            }
            var result = new { success = true, body = data, message = results };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeLeaveCreate(LeaveApplication obj, string ComCode)
        {
            var results = "Cannot Create this Time";

            var data = QueryProcessor.Create(obj, ComCode);
            if(data>0)
            {
                results = "Successfully";
            }
            var result = new { success = true, body = "", message = results };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsLeaveTaken(string date, int id, string leaveId, string ComCode)
        {
            var results = "leave already taken";

            var data = QueryProcessor.IsLeaveTaken(date,  id,  leaveId,  ComCode);
            if (data)
            {
                results = "Leave available";
            }
            var result = new { success = true, body = "", message = results };
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteLeave(int id, string ComCode)
        {
            var results = "Cannot delete this time";

            var data = QueryProcessor.Delete( id, ComCode);
            if (data)
            {
                results = "Sucessfully Deleted";
            }
            var result = new { success = true, body = "", message = results };
            return Json(result, JsonRequestBehavior.AllowGet);

        }


    }
}