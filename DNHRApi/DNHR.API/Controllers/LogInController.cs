﻿using QueryProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace DNHR.API.Controllers
{
    public class LogInController : Controller
    {
        private AllQueryProcessor loginprocessor = new AllQueryProcessor();

        [Route("api/LogIn")]

        public JsonResult Login(string isLogout)
        {
            var result = new { success = false, body = "", message = "Please Login First !!" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UserLogin(string userName, string Password)
        {

            string ComCode = string.Empty;
            var data = loginprocessor.loginprocessor.Authenticate(userName, Password);
            if (data != null)
            {

                string code = data.Select(x => x.Company_Code).First();
                string roleName = data.Select(x => x.RoleName).First();
                string[] values = code.Split('-');
                ComCode = values[0];
                if (!string.IsNullOrEmpty(ComCode))
                {
                    var Data = loginprocessor.loginprocessor.UserDetail(userName, Password, ComCode);
                    var result = new { success = true, body = Data, message = "Login Success" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Invalid User Name or Password" };
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                var result = new { success = false, message = "Invalid User Name or Password" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult LogOut()
        {
            Session.Clear();
            var result = new { Success = true, Body = "", Message = "Success" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh"; // Generated at https://www.random.org/strings
        public static string GenerateToken(string username, string password, string ip)
        {
            string hash = string.Join(":", new string[] { username, ip });
            string hashLeft = "";
            string hashRight = "";
            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));
                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username });
            }
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
        }
        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });
            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));
                return Convert.ToBase64String(hmac.Hash);
            }
        }

    }
}