﻿using Entity;
using Entity.Model;
using QueryProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DNHR.API.Controllers
{
    public class AttendanceController : BaseController
    {
        AllQueryProcessor QueryProcessor = new AllQueryProcessor();
        //public JsonResult DailyPresent(string ComCode)
        //{
        //    var data = QueryProcessor.AttendanceQueryProcessor.DailyPresent(ComCode);
        //    var result = new { success = true, body = data, message = "" };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetDailyPresentEmployee(string ComCode, int EmpId)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetPresentEmployess(0, ComCode);

            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId( EmpId, ComCode);

            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(EmpId)).ToList();
            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDailyLateIn(string ComCode, int EmpId)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetLateInEmployess(0, ComCode);
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(EmpId, ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(EmpId)).ToList();
            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDailyEarlyOut(string ComCode, int EmpId)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetEarlyOutEmployess(0, ComCode);
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(EmpId, ComCode);
            // if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(EmpId)).ToList();
            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDailyAbsentEmployee(string ComCode, int EmpId)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetAbsentEmployess(0, ComCode);
            string rolename = QueryProcessor.CompanyQueryProcessor.GetRoleNameByEmpId(EmpId, ComCode);
            //if (rolename.ToUpper() != "Admin".ToUpper() || rolename.ToUpper() != "SuperAdmin".ToUpper())
            if (!((rolename.ToUpper() == "Admin".ToUpper()) || (rolename.ToUpper() == "SuperUser".ToUpper())))
            {
                data = data.Where(x => x.EmpId == Convert.ToInt32(EmpId)).ToList();
            }
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMonthlyLateIn(string ComCode)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetLateInEmployess(30, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }
        public JsonResult GetMonthlyEarlyOut(string ComCode)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetEarlyOutEmployess(30, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMonthlyAbsentEmployee(string ComCode)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetAbsentEmployess(30, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMonthlyPresentEmployee(string ComCode)
        {
            var data = QueryProcessor.AttendanceQueryProcessor.GetPresentEmployess(30, ComCode);
            var result = new { success = true, body = data, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmployeeMonthlyReport(string ComCode, string empId, string Year, string Month, string DateType)
        {
            //int CompanyCode = Convert.ToInt32(ComCode);

            //if (DateType == "Nepali")
            //{
            //    var EngToDateByNepali = NepDateConverter.NepToEng(Convert.ToInt32(toDate.Split('-')[0]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[2]));
            //    toDate = EngToDateByNepali.ToString();

            //    var EngFromDateByNepali = NepDateConverter.NepToEng(Convert.ToInt32(fromDate.Split('-')[0]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[2]));
            //    fromDate = EngFromDateByNepali.ToString();
            //}

            DateTime fromDate = DateTime.Now;
            DateTime toDate = DateTime.Now;
            int month = Convert.ToInt32(Month);
            int year = Convert.ToInt32(Year);

            if (DateType == null || DateType == "English")
            {
                fromDate = new DateTime(Convert.ToInt32(Year), month, 1);
                toDate = new DateTime(Convert.ToInt32(Year), month, DateTime.DaysInMonth(Convert.ToInt32(Year), month));
            }
            else
            {
                fromDate = NepDateConverter.NepToEng(Convert.ToInt32(Year), month, 1);
                int Day = NepDateConverter.DaysByYearMonth(year, month);
                toDate = NepDateConverter.NepToEng(year, month, Day);
            }


            var usrData = QueryProcessor.AttendanceQueryProcessor.GetEmployeeAttendanceDetails(ComCode, empId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));

            
            var Llist = QueryProcessor.AttendanceQueryProcessor.GetLeaveDetails(ComCode, empId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
            var HList = QueryProcessor.AttendanceQueryProcessor.GetHolidayDetails(ComCode, empId, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
            TimeSpan sapn = new TimeSpan();
            TimeSpan wsapn = new TimeSpan();

            List<EmployeeMonthlyAttendanceView> list = new List<EmployeeMonthlyAttendanceView>();
            foreach (var item in usrData)
            {
                List<HolidayDetails> H = new List<HolidayDetails>();
                List<FilteredLeave> Leave = new List<FilteredLeave>();
                Leave = Llist.ToList();
                H = HList.ToList();
                EmployeeMonthlyAttendanceView emp = new EmployeeMonthlyAttendanceView();
                emp.DateEng = item.AttendanceDate.ToString("yyyy-MM-dd");

                string nepalifrmdate = NepDateConverter.EngToNep(item.AttendanceDate.Year, item.AttendanceDate.Month, item.AttendanceDate.Day).ToString();
                var nepDate = nepalifrmdate.Split('/');
                nepalifrmdate = nepDate[0] + '-' + nepDate[1] + '-' + nepDate[2];
                emp.DateNep = nepalifrmdate;
                emp.EmployeeName = item.Emp_Name;
                emp.DeviceCode = item.EmpDeviceCode;
                emp.Department = item.DepartmentName == null ? "-" : item.DepartmentName;
                emp.Designation = item.DesignationName == null ? "-" : item.DesignationName;
                emp.Shift = (item.EShiftStart.ToString(@"hh\:mm") == "00:00") ? "Not Assign" : item.EShiftStart.ToString(@"hh\:mm") +"-"+ item.EShiftEnd.ToString(@"hh\:mm");
                emp.InTime = (string.IsNullOrEmpty(Convert.ToString(item.InTime))) ? "-" : item.InTime.ToString();

                int val = Convert.ToInt32(item.EarlyLateIn);
                TimeSpan result11 = TimeSpan.FromMinutes(val);
                string fromInTimeString = result11.ToString("hh':'mm");
                if (Convert.ToString(val).Contains('-'))
                {
                    fromInTimeString = fromInTimeString + ("Less");
                }
                else
                {
                    fromInTimeString = fromInTimeString + ("More");
                }
                emp.EarlyLateIn = (string.IsNullOrEmpty(item.EarlyLateIn)) ? "-" : fromInTimeString;

                emp.OutTime = (string.IsNullOrEmpty(Convert.ToString(item.OutTime))) ? "-" : item.OutTime.ToString();
                int valOut = Convert.ToInt32(item.EarlyLateOut);
                TimeSpan resultOut = TimeSpan.FromMinutes(valOut);
                string fromOutTimeString = resultOut.ToString("hh':'mm");
                if (Convert.ToString(valOut).Contains('-'))
                {
                    fromOutTimeString = fromOutTimeString + "(Less)";
                }
                else
                {
                    fromOutTimeString = fromOutTimeString + "(More)"; ;

                }
                emp.EarlyLateOut = (string.IsNullOrEmpty(item.EarlyLateOut)) ? "-" : fromOutTimeString;

                TimeSpan diff = Convert.ToDateTime(Convert.ToString(item.EShiftEnd)) - Convert.ToDateTime(Convert.ToString(item.EShiftStart));
                emp.ShiftHours = (item.EShiftStart.ToString(@"hh\:mm") == "00:00") ? "-" : diff.ToString();

                emp.WorkedHours = (item.WorkedHr.ToString() == "00:00:00") ? "-" : item.WorkedHr.ToString();

                int valET = Convert.ToInt32(item.ExtraTime);
                TimeSpan resultET = TimeSpan.FromMinutes(valET);
                string fromETTimeString = resultET.ToString("hh':'mm");
                if (Convert.ToString(valET).Contains('-'))
                {
                    fromETTimeString = fromETTimeString + "(Less)"; ;
                }

                else
                {
                    fromETTimeString = fromETTimeString + "(More)"; ;

                }
                emp.OverTime = (string.IsNullOrEmpty(item.OverTime)) ? "-" : fromETTimeString;

                var status = "";
                if (item.Status != "")
                {
                    if (Leave.Count() != 0)
                    {
                        var LeaveType = "Leave";
                        if (Leave.FirstOrDefault().LeaveDay == 1)
                        {
                            LeaveType = "Leave";
                        }
                        else
                        {
                            LeaveType = "Half Day Leave";
                        }
                        status = LeaveType;
                    }
                    else if (H.Count() != 0)
                    {
                        sapn = sapn - diff;
                        status = H.First().Status;
                    }
                    else if (item.WeeklyOff.Contains(Convert.ToString((int)item.AttendanceDate.DayOfWeek + 1)))
                    {
                        sapn = sapn - diff;
                        status = "Weekend";
                    }
                    else
                    {
                        status = item.Status;
                    }
                }
                else
                {
                    if (item.WeeklyOff.Contains(Convert.ToString((int)item.AttendanceDate.DayOfWeek + 1)))
                    {
                        sapn = sapn - diff;
                        status = "Present On Weekend";

                    }
                    else if (Leave.Count() != 0)
                    {
                        status = "Leave";
                    }
                    else if (H.Count() != 0)
                    {
                        sapn = sapn - diff;
                        status = "Present On Holiday";
                    }
                    else
                    {
                        if (item.EarlyOutAbsent == 1)
                        {
                            status = "Absent(Exceed EarlyOut)";
                        }
                        else if (item.LateInAbsent == 1)
                        {
                            status = "Absent(Exceed Late In)";
                        }
                        else if (item.EShiftStart.ToString() == "00:00:00")
                        {
                            status = "Shift Not Assigned";
                        }
                        else if (item.LateIn == 1 && item.OutTime == null)
                        {
                            status = "Late In & Not Logout";
                        }
                        else if (item.OutTime == null)
                        {
                            status = "Didnot Logout";
                        }
                        else if (item.EarlyOut == 1 && item.LateIn == 1)
                        {
                            status = "Late In & Early Out";
                        }
                        else if (item.EarlyOut == 1)
                        {
                            status = "Early Out";
                        }
                        else if (item.LateIn == 1)
                        {
                            status = "Late In";
                        }
                        else
                        {
                            status = "In Time";
                        }
                    }
                }
                emp.Status = status;
                list.Add(emp);
            }

            StatusCount statusCount = new StatusCount();
            statusCount.LeaveCount = list.Where(x => x.Status == "In Time").Count();
            statusCount.HalfDayLeaveCount = list.Where(x => x.Status == "Half Day Leave").Count();
            statusCount.WeekendCount = list.Where(x => x.Status == "Weekend").Count();
            statusCount.PresentOnWeekendCount = list.Where(x => x.Status == "Present On Weekend").Count();
            statusCount.PresentOnHolidayCount = list.Where(x => x.Status == "Present On Holiday").Count();
            statusCount.AbsentExceedEarlyOutCount = list.Where(x => x.Status == "Absent(Exceed EarlyOut)").Count();
            statusCount.AbsentExceedLateInCount = list.Where(x => x.Status == "Absent(Exceed Late In)").Count();
            statusCount.LateInNotLogoutCount = list.Where(x => x.Status == "Late In & Not Logout").Count();
            statusCount.DidnotLogoutCount = list.Where(x => x.Status == "Didnot Logout").Count();
            statusCount.LateInEarlyOutCount = list.Where(x => x.Status == "Late In & Early Out").Count();
            statusCount.EarlyOutCount = list.Where(x => x.Status == "Early Out").Count();
            statusCount.LateInCount = list.Where(x => x.Status == "Late In").Count();
            statusCount.InTimeCount = list.Where(x => x.Status == "In Time").Count();
            statusCount.AbsentCount = list.Where(x => x.Status == "Absent").Count();

            var Result = new { Count = statusCount, Results = list };
            var result = new { success = true, body = Result, message = "" };
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoteAttendance(string ComCode, string empId, string DeviceCode, string DateTime, string Latitude, string Longitude)
        {
            //int CompanyCode = Convert.ToInt32(ComCode);
            var data = QueryProcessor.AttendanceQueryProcessor.RemoteAttendance(ComCode, empId, DeviceCode, DateTime, Latitude, Longitude);
            var result = new { success = true, body = "", message = data };
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}