﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class FilteredLeave
    {
        public int EmpId { get; set; }
        public DateTime FromDate { get; set; }
        public string Status { get; set; }
        public int LeaveDay { get; set; }
        public int DeptId { get; set; }

    }
}