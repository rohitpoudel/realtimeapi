﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
	public class LoginCredential
	{
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }

	}
}