﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class HolidayDetails
    {
        public int EmpId { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string Status { get; set; }
        public int DeptId { get; set; }
        public string HolidayName { get; set; }

    }
}