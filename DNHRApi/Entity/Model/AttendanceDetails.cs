﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class AttendanceDetails
    {
        public string AllowedLateInTime { get; set; }
        public string AllowedEarlyOutTime { get; set; }
        public string EmpDeviceCode { get; set; }
        public string OverTime { get; set; }
        public string ExtraTime { get; set; }
        public string EarlyLateIn { get; set; }
        public string EarlyLateOut { get; set; }
        public int EarlyOutAbsent { get; set; }
        public int LateInAbsent { get; set; }
        public TimeSpan EShiftStart { get; set; }
        public TimeSpan EShiftEnd { get; set; }
        public DateTime FromDate { get; set; }
        public string WeeklyOff { get; set; }

        public string FromDateNep { get; set; }
        public DateTime ToDate { get; set; }
        public string ToDateNep { get; set; }
        public int EmpId { get; set; }
        public TimeSpan WorkedHr { get; set; }
        public int CuurentLeaveCount { get; set; }
        public int EmployeeAttendanceId { get; set; }
        public string Emp_Name { get; set; }
        public string DesignationName { get; set; }
        public int DeptId { get; set; }

        public string DepartmentName { get; set; }
        public string Status { get; set; }

        public int Code { get; set; }
        public DateTime date { get; set; }
        public string dateNep { get; set; }
        public string DeviceIP { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string AllowedLateTime { get; set; }
        public string AllowedEarlyTime { get; set; }
        public string ShiftStart { get; set; }
        public string IpAddress { get; set; }
        public string ShiftEnd { get; set; }
        public DateTime LogInTime { get; set; }
        public DateTime LoginDate { get; set; }
        public string LoginDateNep { get; set; }
        public int LateIn { get; set; }
        public int EarlyOut { get; set; }
        public DateTime AttendanceDate { get; set; }

    }

    public class AbsentEmployees
    {
        public string EmpDeviceCode { get; set; }
        public string WeeklyOff { get; set; }
        public int EmpId { get; set; }
        public string Emp_Name { get; set; }
        public string DesignationName { get; set; }
        public int DeptId { get; set; }
        public string DepartmentName { get; set; }
        public string Status { get; set; }
        public string photo { get; set; }
        public DateTime date { get; set; }
        public DateTime AttendanceDate { get; set; }

    }

    public class EmployeeMonthlyAttendanceView
    {
        public string DateEng { get; set; }
        public string DateNep { get; set; }
        public string EmployeeName { get; set; }
        public string DeviceCode { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }        
        public string Shift { get; set; }       
        public string InTime { get; set; }      
        public string EarlyLateIn { get; set; }      
        public string OutTime { get; set; }     
        public string EarlyLateOut { get; set; }     
        public string ShiftHours { get; set; }    
        public string WorkedHours { get; set; }   
        public string OverTime { get; set; }   
        public string ExtraTime { get; set; }   
        public string Status { get; set; }
        public StatusCount StatusCount { get; set; }
    }

    public class StatusCount
    {
        public int LeaveCount { get; set; }
        public int HalfDayLeaveCount { get; set; }
        public int WeekendCount { get; set; }
        public int PresentOnWeekendCount { get; set; }
        public int PresentOnHolidayCount { get; set; }
        public int AbsentExceedEarlyOutCount { get; set; }
        public int AbsentExceedLateInCount { get; set; }
        public int LateInNotLogoutCount { get; set; }
        public int DidnotLogoutCount { get; set; }
        public int LateInEarlyOutCount { get; set; }
        public int EarlyOutCount { get; set; }
        public int LateInCount { get; set; }
        public int InTimeCount { get; set; }
        public int AbsentCount { get; set; }
    }

}