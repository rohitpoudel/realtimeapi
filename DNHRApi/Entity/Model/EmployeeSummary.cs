﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.Model
{
    public class EmployeeSummary
    {
        public int WorkedHours { get; set; }
        public int ExtraTime { get; set; }
        public int EmpId { get; set; }
        public string Emp_Name { get; set; }
        public int DeptId { get; set; }
        public int DesgId { get; set; }
        public string DesignationName { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public int WorkingDays { get; set; }
        public int PresentCount { get; set; }
        public int LateInCount { get; set; }
        public int EarlyInCount { get; set; }
        public int EatlyOutCount { get; set; }

        public int LateOutCount { get; set; }

        public decimal LeaveCount { get; set; }
        public int AbsentCount { get; set; }
        public TimeSpan TotalOT { get; set; }
        public int LEA { get; set; }
        public int WeekendCount { get; set; }
        public int HolidayCount { get; set; }
        public int WorkedOnHoliday { get; set; }
        public int WorkedOnWeekend { get; set; }

    }
}