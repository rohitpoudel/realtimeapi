﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Entity.Model
{
    public class User
    {
        public Int64 SNo { get; set; }

        public int Id { get; set; }
        [Required]
        [Display(Name = "User Role Name")]    
        public int UserRoleId { get; set; }
        [Required]
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }
        public string Password { get; set; }
        [Required]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public char Event { get; set; }
        
        public string UserName { get; set; }
        public string Emp_Name { get; set; }
        public string ROLE_NAME { get; set; }
        public string Company_Code { get; set; }


        public List<SelectList> userList { get; set; }
    }
}